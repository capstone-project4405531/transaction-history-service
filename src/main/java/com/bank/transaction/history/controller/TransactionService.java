package com.bank.transaction.history.controller;

import com.bank.transaction.history.Transaction;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionService {
    private List<Transaction> transactionList;

    @PostConstruct
    public void init() {
        transactionList = new ArrayList<>();
        for (int i = 0; i < 3; ++i) {
            Transaction tnx1 = new Transaction();
            tnx1.setFromUser("user" + i);
            tnx1.setRemark("Thanh toan " + i);
            tnx1.setToUser("user1_" + i);
            tnx1.setRanferTime(Instant.now());
            transactionList.add(tnx1);
        }
    }

    public void add(Transaction tnx) {
        transactionList.add(tnx);
    }

    public List<Transaction> getTxnList() {
        return transactionList;
    }
}
