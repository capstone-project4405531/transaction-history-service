package com.bank.transaction.history;

import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
public class Transaction {
    private String fromUser;
    private String toUser;
    private String remark;
    private Instant ranferTime;
}
